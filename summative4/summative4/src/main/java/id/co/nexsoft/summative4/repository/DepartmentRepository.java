package id.co.nexsoft.summative4.repository;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.summative4.model.Department;

public interface DepartmentRepository extends CrudRepository<Department, Integer>{
	Department findById(int id);
	Department findByName(String name);
	Department save(Department department);
}
