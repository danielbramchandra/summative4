package id.co.nexsoft.summative4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController {
	@Autowired
	UserRepository userRepo;
	@Autowired
	DepartmentRepository departmentRepo;

	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("users", userRepo.findAll());
		return "index";
	}

	@RequestMapping(value = "/user/delete/{id}", method = RequestMethod.GET)
	public String deleteUser(@PathVariable(value = "id") int id) {
		userRepo.deleteById(id);
		return "redirect:/";
	}

	@PostMapping(value = "/add", consumes = "application/json")
	public String add(@RequestBody User user) {
		Department isdepartmentNameExist = departmentRepo.findByName(user.getDepartment().getName());
		if (isdepartmentNameExist != null) {
			user.setDepartment(isdepartmentNameExist);
		} else {
			departmentRepo.save(user.getDepartment());
		}
		userRepo.save(user);
		return "redirect:/";
	}

	@PostMapping(value = "/user/edit/", consumes = "application/json")
	public String updateData(@RequestBody User userFront) {
		System.out.println(userFront.getEmail());
		User usrDB = userRepo.findByEmail(userFront.getEmail());
		usrDB.setName(userFront.getName());
		usrDB.setLastname(userFront.getLastname());
		usrDB.setEmail(userFront.getEmail());
		usrDB.setBirthdate(userFront.getBirthdate());
		usrDB.setDepartment(userFront.getDepartment());
		userRepo.save(usrDB);		
		return "redirect:/";
	}

	@PostMapping(value = "/addAll", consumes = "application/json")
	public String addAll(@RequestBody List<User> user) {
		List<User> temp = new ArrayList<User>();

		for (Iterator iterator = user.iterator(); iterator.hasNext();) {
			User user2 = (User) iterator.next();
//			System.out.println(user2.getName()+" "+user2.getEmail());
			if (user2.getName().isBlank() || user2.getLastname().isBlank() || user2.getEmail().isBlank()
					|| user2.getBirthdate().isBlank() || user2.getDepartment().getName().isBlank()) {
				continue;
			}
			Department isdepartmentNameExist = departmentRepo.findByName(user2.getDepartment().getName());
			if (isdepartmentNameExist != null) {
				user2.setDepartment(isdepartmentNameExist);
			} else {
				departmentRepo.save(user2.getDepartment());
			}
			temp.add(user2);
		}

		if (temp.size() != 0) {
			userRepo.saveAll(temp);
		}

		return "redirect:/";
	}

}
