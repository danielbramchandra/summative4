package id.co.nexsoft.summative4;

import org.springframework.data.repository.CrudRepository;

public interface DepartmentRepository extends CrudRepository<Department, Integer>{
	Department findById(int id);
	Department findByName(String name);
	Department save(Department department);
}
