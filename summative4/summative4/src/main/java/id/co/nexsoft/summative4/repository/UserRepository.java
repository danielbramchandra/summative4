package id.co.nexsoft.summative4.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.summative4.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	List<User> findAll();
	User save(User user);	
	User findById(int id);
	User findByEmail(String email);
	void deleteById(int id);
	void deleteByEmail(String email);
}
